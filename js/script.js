const head = document.getElementsByTagName('head')[0],
    body = document.querySelector('.about').parentElement,
    themeSwitch = document.querySelector('#theme-switch'),
    ctaButton = document.querySelector('#cta-button'),
    confirmButton = document.querySelector('.button-confirm'),
    modalFormContainer = document.querySelector('#modal-form-container'),
    modalSuccessContainer = document.querySelector('#modal-success-container'),
    modalFormButtonExit = document.querySelector('#modal-form-button-exit'),
    modalSuccessButtonExit = document.querySelector('#modal-success-button-exit'),
    form = document.querySelector('#modal-form-container form'),
    backButton = document.querySelector('.go-back-button'),
    MODAL_ACTIVE_CLASS = 'modal-active';
console.log(body);
let currentTheme = 'dark',
    link = document.createElement('link');
function closeModals(e) {
    e.preventDefault();
    modalFormContainer.classList.remove(MODAL_ACTIVE_CLASS);
    modalSuccessContainer.classList.remove(MODAL_ACTIVE_CLASS);
    body.classList.remove(MODAL_ACTIVE_CLASS);

}
function openSuccessModal() {
    modalFormContainer.classList.remove(MODAL_ACTIVE_CLASS);
    modalSuccessContainer.classList.add(MODAL_ACTIVE_CLASS);
    modalSuccessButtonExit.addEventListener('click', closeModals);
}
function sendUserInfo(e) {
    e.preventDefault();
    const formData = new FormData(form);
    fetch('/', {
        method: 'POST',
        headers: {
            'Content-Type': `application/x-www-form-urlencoded`
        },
        body: new URLSearchParams(formData).toString()
    }).then(openSuccessModal);
}
ctaButton.addEventListener('click', function () {
    modalFormContainer.classList.add(MODAL_ACTIVE_CLASS);
    body.classList.add(MODAL_ACTIVE_CLASS);
    modalFormButtonExit.addEventListener('click', closeModals);
    form.addEventListener('submit', sendUserInfo);
});
window.onscroll = () => {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        backButton.style.display = 'block';
    } else {
        backButton.style.display = 'none';
    }
};
backButton.addEventListener('click', function () {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
});
const _setColorTheme = (color) => {
    link.rel = 'stylesheet';
    link.href = `css/color.${color}.css`;
    head.appendChild(link);
};
_setColorTheme(currentTheme);
themeSwitch.addEventListener('change', () => {

    if (currentTheme === 'dark') {
        _setColorTheme('light');
        currentTheme = `light`;
    } else {
        _setColorTheme('dark');
        currentTheme = `dark`;
    }
});